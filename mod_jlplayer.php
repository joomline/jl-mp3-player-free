<?php
/**
 * JoomLine mp3 player - Joomla mp3 player
 *
 * @version 1.5
 * @package JoomLine mp3 player
 * @author Anton Voynov (anton@joomline.ru), Sergii Gaievskiy (shturman.kh@gmail.com)
 * @copyright (C) 2010 by Anton Voynov(http://www.joomline.ru)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 * If you fork this to create your own project,
 * please make a reference to JoomLine someplace in your code
 * and provide a link to http://www.joomline.ru
 **/
defined('_JEXEC') or die('Restricted access');

$music_dir = $params->get('music_dir');
$server_utf8 = $params->get('server_utf8');
$button_select = $params->get('button_select');
$shfl = $params->get('shuffle');
$base_uri = JURI::base();
?>
<script type="text/javascript">
	var enable_autoplay = false;
	var baseuri = '<?=$base_uri?>';
	Joomla.openInNewWindow = function(stop){
        if (stop) {
            jQuery("#jquery_jplayer").jPlayer("stop");
        }
        newjlwindow = window.open("<?=$base_uri?>/modules/mod_jlplayer/standalone.php","JLPlayer","width=300,height=300,left=400,top=150,toolbar=no,location=no,directories=no,menubar=no,status=no,fullscreen=no,scrollbars=yes,resize=yes");
    }
</script>
<?php if($button_select == 1) { ?>
    <center>
        <a href="javascript:Joomla.openInNewWindow(false)">
            <img src="<?=$base_uri?>/modules/mod_jlplayer/skin/btn_play.png" alt=""/>
        </a>
    </center>
<?php } elseif($button_select == 0) { ?>
    <?php include_once('player.php') ?>
    <center>
        <a href="javascript:Joomla.openInNewWindow(true)">
            Open in new window 
        </a>
    </center>
<?php } ?>

	<div style="text-align: right;">
			<a style="text-decoration:none; color: #c0c0c0; font-family: arial,helvetica,sans-serif; font-size: 5pt; " target="_blank" href="http://joomline.org/">http://joomline.org/</a>
	</div>
