<?php
/**
 * JoomLine mp3 player - Joomla mp3 player
 *
 * @version 1.5
 * @package JoomLine mp3 player
 * @author Anton Voynov (anton@joomline.ru), Sergii Gaievskiy (shturman.kh@gmail.com)
 * @copyright (C) 2010 by Anton Voynov(http://www.joomline.ru)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 * If you fork this to create your own project,
 * please make a reference to JoomLine someplace in your code
 * and provide a link to http://www.joomline.ru
 **/

defined('_JEXEC') or die('Restricted access');

function ascii2hex($ascii, $reverse = false) {
	$hex = array();
	for ($i = 0; $i < strlen($ascii); $i++) {
		$byte = strtoupper(dechex(ord($ascii{$i})));
		$byte = str_repeat('0', 2 - strlen($byte)).$byte;
		$hex[] = $byte;
	}
	if ($reverse) $hex = array_reverse($hex);
	return implode(" ",$hex);
}

function read_frame (&$f, &$tagdata, $frame) {
	$pos = strpos($tagdata,$frame);
	if ( $pos !== FALSE) {
		// frame found. read length of this frame
		fseek($f, 10+$pos+4);
		$frame2len = hexdec(ascii2hex(fread($f,4)));
		if (($frame2len-1) > 0) {
			// read frame data
			fseek($f, 10+$pos+4+2+4+1);
			$data = trim(fread($f,$frame2len-1));

			$hexfdata = ascii2hex($data);
				if ( substr($hexfdata,0,5) == 'FF FE' or substr($hexfdata,0,5) == 'FE FF' ) {
					$data = iconv("UCS-2","UTF-8",$data);
				} else {
					if (!preg_match('//u', $data)) {
						$data = iconv("cp1251", "UTF-8",$data);
			}
				}
			return $data;
		} else {
			return false;
		}
	} else {
		return false;
	}
	
}

function readmp3tag($file) {
	$f = fopen($file, 'rb');
	rewind($f);
	fseek($f, -128, SEEK_END);
	$tmp = fread($f,128);
	if ($tmp[125] == Chr(0) and $tmp[126] != Chr(0)) {
		// ID3 v1.1
		$format = 'a3TAG/a30NAME/a30ARTISTS/a30ALBUM/a4YEAR/a28COMMENT/x1/C1TRACK/C1GENRENO';
	} else {
		// ID3 v1
		$format = 'a3TAG/a30NAME/a30ARTISTS/a30ALBUM/a4YEAR/a30COMMENT/C1GENRENO';
	}
	$id3v1tag = unpack($format, $tmp);

	// read tag length
	fseek($f, 8);
	$tmp = fread($f,2);
	$tmp = ascii2hex($tmp);
	$taglen= hexdec($tmp);
	$tagdata = "";
	if ($taglen > 0) {
	//read tag data
	fseek($f, 10);
	$tagdata = fread($f,$taglen);
	}
	
	// find song title frame
	$title = read_frame ($f, $tagdata, "TIT2");
	if (!$title) {
		if ($id3v1tag['TAG']== 'TAG' && ascii2hex(substr($id3v1tag['NAME'],0,1)) != '00' ) {
			$title = $id3v1tag['NAME'];
		} else {
		$title = explode(DS,$file);
		$title = $title[count($title)-1];
		$title = explode('.',$title);
		$title=$title[0];
	}
		if (!preg_match('//u', $title)) $title = iconv("cp1251", "UTF-8",$title);
	}
	$artist = read_frame ($f, $tagdata, "TPE1");
	if (!$artist) {
		if ($id3v1tag['TAG']== 'TAG' &&  ascii2hex(substr($id3v1tag['ARTISTS'],0,1)) != '00') {
			$artist = $id3v1tag['ARTISTS'];
		} else {
			$artist = "";
	}
	}
	if (!preg_match('//u', $artist)) $artist = iconv("cp1251", "UTF-8//TRANSLIT",$artist);
	
	$id3tag['NAME']		= $title;
	$id3tag['ARTIST']	= $artist;

	return $id3tag;
}

if (DS == "/") 
	$dir = str_replace("\\",DS,$music_dir);
else 
	$dir = str_replace("/",DS,$music_dir);
$dir = JPATH_ROOT.DS.$dir;


if (!is_dir($dir)) {
	echo "Wrong dir in settings";
} else {
	$files = glob($dir.DS."*.{mp3,MP3}",GLOB_BRACE);
	if (count($files) > 0) {
		sort($files);
		$host = $base_uri;
		foreach ($files as $file) {
			$tags = readmp3tag($file);
			$file = explode (DS, $file);
			if ($server_utf8 == 1) {
				$fname = rawurlencode(iconv("cp1251","utf-8",$file[count($file)-1]));
			} else {
				$fname = rawurlencode($file[count($file)-1]);
			}
			
			$file = $host."/".$music_dir."/".$fname;
			$artist = trim($tags['ARTIST']);
			$artist = $artist == "" ? "" : "{$tags['ARTIST']} - ";
			$playlist[] = '{name:"'.$artist.$tags['NAME'].'",mp3:"'.$file.'"}';
		}
	}
	/*
	 * 	
	//if(!window.jQuery) {
		document.write(unescape('<script type="text/javascript" src="<?=$base_uri?>/modules/mod_jlplayer/js/jq.js">%3C/script%3E'));
		document.write(unescape('<script type="text/javascript">jQuery.noConflict();%3C/script%3E'));
	//}

	 * 
	 */
	?>
	<script type="text/javascript">
	var myPlayList = [
		<?php 
			echo implode(",\n		",$playlist)."\n"; 
		?>
		];
		Array.prototype.find=function(v){
				for (i=0;i<this.length;i++){
						if (this[i]==v) return i;
				}
				return 0;
		}
		var plIndex = [];
		for (i=0;i<myPlayList.length;i++) {
				plIndex[i] = i;
		}
		<?php if ($shfl == 1) : ?>
		//shuffle
		function randOrd(){
			return (Math.round(Math.random())-0.5);
		}
		plIndex.sort(randOrd);
		<?php endif; ?>
		function setCookie (name, value) {
					document.cookie = name + "=" + escape(value) + "; expires=Thu, 01-Jan-2055 00:00:01 GMT; path=/";
		}

		function getCookie(name) {
			var cookie = " " + document.cookie;
			var search = " " + name + "=";
			var setStr = null;
			var offset = 0;
			var end = 0;
			if (cookie.length > 0) {
				offset = cookie.indexOf(search);
				if (offset != -1) {
					offset += search.length;
					end = cookie.indexOf(";", offset)
					if (end == -1) 	{
						end = cookie.length;
					}
					setStr = unescape(cookie.substring(offset, end));
				}
			}
			return(setStr);
		}		
		
		function changeShflStatus(el) {
			nowPlay = plIndex[playItem];
			
			if (el.checked) {
				setCookie("jlp_shfl","shuffle");
				plIndex.sort(randOrd);
			} else {
				setCookie("jlp_shfl","notshuffle");
				plIndex.sort();
			}
			playItem = plIndex.find(nowPlay);
			 
		}
		
	</script>	
	<script type="text/javascript" src="<?=$base_uri?>/modules/mod_jlplayer/js/jq.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<link href="<?=$base_uri?>/modules/mod_jlplayer/skin/skin.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?=$base_uri?>/modules/mod_jlplayer/js/jquery.jplayer.min.js"></script>
	
	<script type="text/javascript">
		var playItem = 0;
	 
		jQuery(function(){
			var jpPlayTime = jQuery("#jplayer_play_time");
			var jpTotalTime = jQuery("#jplayer_total_time");
			var jlp_shfl = getCookie("jlp_shfl");
			if (jlp_shfl == "shuffle") {
				document.getElementById('jlp_shfl').checked = true;
			} else if (jlp_shfl == "notshuffle") {
				document.getElementById('jlp_shfl').checked = false;
			}
			jsuri = baseuri+"/modules/mod_jlplayer/js/";
			jQuery("#jquery_jplayer").jPlayer({
				ready: function() {
					displayPlayList();
					playListInit(enable_autoplay); // Parameter is a boolean for autoplay.
				},
				errorAlerts:true,
				warningAlerts:true,
				swfPath: jsuri
			})
			.jPlayer("onProgressChange", function(loadPercent, playedPercentRelative, playedPercentAbsolute, playedTime, totalTime) {
				jpPlayTime.text(jQuery.jPlayer.convertTime(playedTime));
				jpTotalTime.text(jQuery.jPlayer.convertTime(totalTime));
			})
			.jPlayer("onSoundComplete", function() {
				playListNext();
			});
		 
			
			
			jQuery("#jplayer_previous").click( function() {
				playListPrev();
				return false;
			});
		 
			jQuery("#jplayer_next").click( function() {
				playListNext();
				return false;
			});
			
			
		});	
		
		
		function displayPlayList() {
			for (i=0; i < myPlayList.length; i++) {
				jQuery("#jplayer_playlist").append("<div id='jplayer_playlist_item_"+i+"'>"+ myPlayList[i].name +"</div>");
				jQuery("#jplayer_playlist_item_"+i).data( "index", i ).click( function() {
					var index = jQuery(this).data("index");
					if (plIndex[playItem] != index) {
						_index = plIndex.find(index);
						playListChange( _index, index );
					} else {
						jQuery("#jquery_jplayer").jPlayer("play");
					}
				});
			}
		}
	 
		function playListInit(autoplay) {
			if(autoplay) {
				playListChange(0, plIndex[0] );
			} else {
				playListConfig(0, plIndex[0] );
			}
		}
	 
		function playListConfig(_index, index ) {
			jQuery("#jplayer_playlist_item_"+plIndex[playItem]).removeClass("jplayer_playlist_current");
			jQuery("#jplayer_playlist_item_"+index).addClass("jplayer_playlist_current");
			playItem = _index;
			jQuery("#jquery_jplayer").jPlayer("setFile", myPlayList[plIndex[playItem]].mp3);
		}
	 
		function playListChange(_index, index ) {
			playListConfig(_index, index );
			jQuery("#jquery_jplayer").jPlayer("play");
		}

		function playListNext() {
			var _index = (playItem+1 < myPlayList.length) ? playItem+1 : 0;
			var index = plIndex[_index];
			playListChange(_index, index );
		}
	 
		function playListPrev() {
			var _index = (playItem-1 >= 0) ? playItem-1 : myPlayList.length-1;
			var index = plIndex[_index];
			playListChange(_index, index );
		}	
	
	</script>
	
	<?php include_once(JPATH_ROOT.DS.'modules/mod_jlplayer/skin/tpl.php'); ?> 
	
	
	<?php
}
