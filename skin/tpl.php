<div id="jquery_jplayer"></div>

<div class="jp-playlist-player">
		<div class="jp-interface">
				<div class="jp-controls">
						<table width="90%" style="margin-top: 5px">
								<tr>
										
										<td>
												<div style="float: left; width: 24%;"><div id="jplayer_previous" class="jlp-previous">&nbsp;</div></div>
												<div style="float: left; width: 24%;">
														<div id="jplayer_play" class="jlp-play">&nbsp;</div>
														<div id="jplayer_pause" class="jlp-pause">&nbsp;</div>
												</div>
												<div style="float: left; width: 24%;"><div id="jplayer_stop" class="jlp-stop">&nbsp;</div></div>
												<div style="float: left; width: 24%;"><div id="jplayer_next" class="jlp-next">&nbsp;</div></div>
										</td>

								</tr>
						</table>

						
						<table width="100%" style="margin-top: 5px">
								<tr>
										<td width="50"><div id="jplayer_play_time" class="jp-play-time">&nbsp;</div></td>
										<td>
												<div class="jp-progress">
														<div id="jplayer_load_bar" class="jp-load-bar">
																<div id="jplayer_play_bar" class="jp-play-bar"></div>
														</div>
												</div>
										</td>
										<td width="50"><div id="jplayer_total_time" class="jp-total-time">&nbsp;</div></td>
								</tr>
						</table>

						

						<table width="100%">
								<tr>
										<td width="28"><div id="jplayer_volume_min" class="jlp-volume-min">&nbsp;</div></td>
										<td>
												<div id="jplayer_volume_bar" class="jp-volume-bar">
														<div id="jplayer_volume_bar_value" class="jp-volume-bar-value"></div>
												</div>
										</td>
										<td width="28"><div id="jplayer_volume_max" class="jlp-volume-max">&nbsp;</div></td>
								</tr>
						</table>
						<table width="100%" style="margin-top: 5px">
								<tr>
										<td style="font-size:9pt; font-weight:bold" align="center">
											<input type="checkbox" id="jlp_shfl" <?=$shfl == 1 ? "checked" : ""?> onchange="changeShflStatus(this)"><label for="jlp_shfl">Random playback</label>
										</td>
								</tr>
						</table>
						
				</div>


		</div>

		<div id="jplayer_playlist" class="jp-playlist">

		</div>
</div>
