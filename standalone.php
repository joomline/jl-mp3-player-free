<?php
/**
 * JoomLine mp3 player - Joomla mp3 player
 *
 * @version 1.5
 * @package JoomLine mp3 player
 * @author Anton Voynov (anton@joomline.ru), Sergii Gaievskiy (shturman.kh@gmail.com)
 * @copyright (C) 2010 by Anton Voynov(http://www.joomline.ru)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 * If you fork this to create your own project,
 * please make a reference to JoomLine someplace in your code
 * and provide a link to http://www.joomline.ru
 **/
define( '_JEXEC', 1 );

chdir("../../");
define('JPATH_BASE', getcwd() );
$ss = JPATH_BASE;
define( 'DS', DIRECTORY_SEPARATOR );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe =& JFactory::getApplication('site');

$db = JFactory::getDBO();

$db->setQuery("SELECT `params` FROM #__modules WHERE `module`='mod_jlplayer'");
$_prow = $db->loadRow();
$_prow = explode("\n", $_prow[0]);
foreach ( $_prow as $row) {
	$row = explode("=",$row);
	$params[$row[0]] = $row[1];
}
preg_match("|(.*)\/modules\/mod_jlplayer|", JURI::base(), $matches);
$base_uri = $matches[1];
$music_dir = "music";
$server_utf8 = $params['server_utf8'];
$enable_autoplay = $params['button_select'] == 1 ? "true" : "false";
$shfl = $params['shuffle'];
header("Content-Type: text/html; charset=UTF-8 ");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Lang" content="ru">
	<title>JL Player</title>
</head>
<body>
    <script type="text/javascript">
    var enable_autoplay = <?=$enable_autoplay?>;
	var baseuri = '<?=$base_uri?>';
    </script>
<?php include_once('player.php'); ?>  
	<div style="text-align: center;">
		<em>
			<span style="color: #c0c0c0; font-family: arial,helvetica,sans-serif; font-size: 5pt;">
				<a target="_blank" href="http://joomline.org/">http://joomline.org/</a>
			</span>
		</em>
	</div>

</body>
</html>
